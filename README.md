# Microservices and Domain Driven Design / Hexagonal architecture with NestJs

## Purpose
Follow and implement the Domain Driven Design / Hexagonal architecture guidelines for a large scale web application using NestJs.

Each microservice may be organized as below :
![DDD architecture schema](doc/images/ddd_architecture.jpg "DDD architecture schema")

## The project example domain
As a dumb example for experiment, this project try to represent the development of Bank SaaS application.

Event Storming (at his actual state) :
![Event Storming](doc/images/event_storming.png "Event Storming")


## Setup

### prerequisites
- [docker](https://docs.docker.com/engine/install/) installed
- [docker-compose](https://docs.docker.com/compose/install/) installed

### launch
Clone the project and setup docker containers and volumes in one line :
``` bash
./up_all.sh
```

### check and debug
1) api

Logs of the api gateway can be check for debug :
``` bash
cd api
docker-compose logs -f app
```
2) microservices

Logs of each microservices can be used to check and debug :
Example : 
``` bash
cd users-ms
docker-compose logs -f app
```
3) rabbitmq

Logs of rabbitmq can be check for debug :
``` bash
cd rabbitmq
docker-compose logs -f
```

### shutdown
``` bash
./down_all.sh
```

## Microservice architecture of the application

Modules of the Api Gateway are organized to call the corresponding microservice via message Queuing (RabbitMQ).
Each microservice can call an other microservice via message Queuing too (RabbitMQ).
There's no authentication service at the moment.

The Api gateway can be accessed via port 3000.

## API requests

Hello world :
``` bash
curl http://localhost:3000/ # should display "Hello World!"
```

Create a user :
``` bash
curl --location --request POST 'http://localhost:3000/users' \
--header 'Content-Type: application/json' \
--data-raw '{
    "firstName": "toto",
    "lastName": "tutu",
    "email": "tutu@example.com",
    "password": "secret"
}'
# should display "OK"
```
List users :
``` bash
curl --location --request GET 'http://localhost:3000/users'
# should display "[
#     {
#         "id": 1,
#         "firstName": "toto",
#         "lastName": "tutu",
#         "email": "toto@example.com"
#     },
#     {
#         "id": 4,
#         "firstName": "toto",
#         "lastName": "tutu",
#         "email": "tutu@example.com"
#     }
# ]"
```

## Functionnalities
### Done
- Api Gateway with a GET an POST example
- DDD architecture components :
  - Interface
    - Controllers
    - DTOs
    - CQRS example with at least
      - 1 command
      - 1 query
  - Core
    - Application service
    - Domain service
    - Entities
    - Aggregate
  - Infrastructure
    - Repositories (DB connexion)
    - Microservice (RabbitMQ message queuing)

### TODO
- Add Authentification service and guard permission management
- Implement a SAGA example
- Add a Bank microservice in charge to Accounts and Clients logic
- ...