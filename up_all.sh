#!/bin/bash

echo "up all..."

docker network create rabbitmq-net

cd rabbitmq
sudo mkdir -p docker/rabbitmq/data
sudo mkdir -p docker/rabbitmq/log
sudo chown 100:${USER} docker/rabbitmq
sudo chown 100:${USER} docker/rabbitmq/data
sudo chown 100:${USER} docker/rabbitmq/log
docker-compose up -d
cd ..

cd api
sudo mkdir -p docker/mariadb
sudo chown 999:999 docker/mariadb
docker-compose up -d
cd ..

cd users-ms
sudo mkdir -p docker/mariadb
sudo chown 999:999 docker/mariadb
docker-compose up -d
cd ..

echo "all is up !"
