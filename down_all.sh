#!/bin/bash

echo "down all..."

cd users-ms && docker-compose down && cd ..

cd api && docker-compose down && cd ..

cd rabbitmq && docker-compose down && cd ..

docker network rm rabbitmq-net

echo "all is down !"
