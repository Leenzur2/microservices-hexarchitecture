import { Body, Controller, Get, Inject, Post, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Observable, timeout } from 'rxjs';
import ValidationPipe from 'src/validation.pipe';
import { UserSignUpInputDTO } from './DTOs/user-sign-up.input.dto';

const logger = new Logger('Api:UsersController');

@Controller('users')
export class UsersController {
  constructor(
    @Inject('MS_USERS_SERVICE') private client: ClientProxy,
  ) {}


  @Get()
  getUsers(): Observable<any> {
    logger.debug('getUsers requested')
    return this.client.send({cmd: 'ms-users-get-all-users'}, '').pipe(timeout(3000));
  }
  

  @Post()
  signUpUser(@Body(new ValidationPipe()) signUpData: UserSignUpInputDTO): Observable<any> {
    logger.debug('Post signUpUser requested')
    logger.debug('Calling ms-users-signup-user ...')
    return this.client.send({cmd: 'ms-users-signup-user'}, JSON.stringify(signUpData)).pipe(timeout(3000));
  }
}
