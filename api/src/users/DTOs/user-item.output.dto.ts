import { IsNumber, IsString } from 'class-validator';

export class UserItemOutputDTO {

    @IsNumber()
    id: number;

    @IsString()
    firstName: string;

    @IsString()
    lastName: string;
}