import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { UsersController } from './users.controller';

  const rabbitmqUrl = `amqp://${process.env.RABBITMQ_HOSTNAME}:${process.env.RABBITMQ_PORT}`;
  
  @Module({
  imports: [
    CqrsModule,
    ClientsModule.register([
      {
        name: 'MS_USERS_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: [rabbitmqUrl],
          queue: 'ms-users-service_queue',
          queueOptions: {
            durable: false
          },
        },
      },
    ]),
  ],
  providers: [],
  controllers: [UsersController],
  exports: [ClientsModule],
})
export class UsersModule {}
