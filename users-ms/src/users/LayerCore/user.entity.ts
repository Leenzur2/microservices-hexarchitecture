import { Entity, PrimaryGeneratedColumn, Column, Unique } from "typeorm";

@Entity()
@Unique(['email'])
export class UserEntity {
  
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @Column({ length: 45 })
  firstName: string = undefined;

  @Column({ length: 45 })
  lastName: string = undefined;

  @Column({ length: 100 })
  email: string = undefined;

  @Column({ length: 45 })
  password: string = undefined;

  @Column({ default: true })
  isActive: boolean = undefined;

}
  