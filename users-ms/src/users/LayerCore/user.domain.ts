import { Injectable } from "@nestjs/common";
import { AggregateRoot } from "@nestjs/cqrs";
import { InputMapper } from "../../helpers/input.mapper";
import { UserSignUpInputDTO } from "../LayerInterface/DTOs/user-sign-up.input.dto";
import { UserEntity } from "./user.entity";
import { UserPort } from "../LayerInfrastructure/Ports/user-port";

@Injectable()
export class UserDomain extends AggregateRoot {

  constructor(
    private userPort: UserPort,
    private userInputMapper: InputMapper<UserSignUpInputDTO, UserEntity>,
  ) { super(); }

  async create(signUpData: UserSignUpInputDTO) {
    const user = this.userInputMapper.map(signUpData, new UserEntity());
    if (await this.userPort.userAlreadyExists(user)) {
      throw new Error(`User ${user.email} try to sign up but already exists`);
    }

    this.userPort.saveUser(user);
  }
}
  