import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserController } from './LayerInterface/user.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { UserSignUpHandler } from './LayerInterface/CQRS/Handlers/user-sign-up.handler';
import { UserItemAllHandler } from './LayerInterface/CQRS/Handlers/user-item.all.handler';
import { UserDomain } from './LayerCore/user.domain';
import { UserRepository } from './LayerInfrastructure/Repositories/user.repository';
import { InputMapper } from '../helpers/input.mapper';
import { UserEntity } from './LayerCore/user.entity';
import { OutputMapper } from 'src/helpers/output.mapper';
import { UserPort } from './LayerInfrastructure/Ports/user-port';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([UserEntity]),
  ],
  controllers: [UserController],
  providers: [
    OutputMapper,
    InputMapper,
    UserPort,
    UserRepository,
    UserSignUpHandler,
    UserItemAllHandler,
    UserDomain,
  ],
})
export class UsersModule {}
