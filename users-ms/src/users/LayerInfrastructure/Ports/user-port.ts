import { Injectable, Logger } from '@nestjs/common';
import { UserEntity } from 'src/users/LayerCore/user.entity';
import { UserRepository } from '../Repositories/user.repository';

const logger = new Logger('MS-User:UserPort');


@Injectable()
export class UserPort {

  constructor(
    private readonly userRepo: UserRepository,
  ) {}

  async getAllUsers() {
    return await this.userRepo.find();
  }

  async getUser(id: number) {
    return await this.userRepo.findOneBy({ id });
  }

  async userAlreadyExists({ email }: UserEntity): Promise<boolean> {
    const [entities, count] = await this.userRepo.findAndCountBy({ email });
    return (count > 0);
  }

  async saveUser(userEntity: UserEntity) {
    await this.userRepo.save(userEntity);
  }
}
