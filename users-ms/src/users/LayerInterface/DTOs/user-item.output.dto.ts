import { IsNumber, IsString } from 'class-validator';

export class UserItemOutputDTO {

    @IsNumber()
    id: number = undefined;

    @IsString()
    firstName: string = undefined;

    @IsString()
    lastName: string = undefined;

    @IsString()
    email: string = undefined;
}