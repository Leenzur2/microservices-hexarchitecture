import { IsNumber } from 'class-validator';

export class UserItemByIdInputDTO {

    @IsNumber()
    readonly id: number = undefined;

}