import { IsEmail, IsString } from 'class-validator';

export class UserSignUpInputDTO {

    @IsString()
    readonly firstName: string = undefined;

    @IsString()
    readonly lastName: string = undefined;

    @IsEmail()
    readonly email: string = undefined;

    @IsString()
    readonly password: string = undefined;
}