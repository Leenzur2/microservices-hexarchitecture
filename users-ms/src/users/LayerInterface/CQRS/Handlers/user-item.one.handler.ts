import { Logger } from '@nestjs/common';
import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { UserItemOutputDTO } from '../../DTOs/user-item.output.dto';
import { UserItemOneQuery } from '../Queries/user-item.one.query';
import { UserEntity } from '../../../LayerCore/user.entity';
import { OutputMapper } from 'src/helpers/output.mapper';
import { UserPort } from 'src/users/LayerInfrastructure/Ports/user-port';

const logger = new Logger('MS-User:UserItemOneHandler');

@QueryHandler(UserItemOneQuery)
export class UserItemOneHandler implements IQueryHandler<UserItemOneQuery> {

  constructor(
    private readonly userPort: UserPort,
    private readonly outputMapper: OutputMapper<UserEntity, UserItemOutputDTO>,
  ){}

  async execute({ inputDTO }:  UserItemOneQuery): Promise<UserItemOutputDTO> {
    logger.debug('execute OK')
    const { id } = inputDTO;
    const entity = await this.userPort.getUser(id);
    return await this.outputMapper.map(entity, new UserItemOutputDTO());
  }
}
