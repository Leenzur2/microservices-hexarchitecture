import { Logger } from '@nestjs/common';
import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { UserItemOutputDTO } from '../../DTOs/user-item.output.dto';
import { UserItemAllQuery } from '../Queries/user-item.all.query';
import { UserEntity } from '../../../LayerCore/user.entity';
import { OutputMapper } from 'src/helpers/output.mapper';
import { UserPort } from 'src/users/LayerInfrastructure/Ports/user-port';

const logger = new Logger('MS-User:UserItemAllHandler');

@QueryHandler(UserItemAllQuery)
export class UserItemAllHandler implements IQueryHandler<UserItemAllQuery> {

  constructor(
    private readonly userPort: UserPort,
    private readonly mapper: OutputMapper<UserEntity, UserItemOutputDTO>,
    ){}

  async execute(): Promise<UserItemOutputDTO[]> {
    logger.debug('execute OK')

    const entities = await this.userPort.getAllUsers();
    return await entities.map(entity => this.mapper.map(entity, new UserItemOutputDTO()));
  }
}
