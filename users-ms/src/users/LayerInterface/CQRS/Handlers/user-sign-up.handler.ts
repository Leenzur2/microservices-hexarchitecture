import { Inject, Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { UserSignUpCommand } from '../Commands/user-sign-up.command';
import { UserDomain } from '../../../LayerCore/user.domain';

const logger = new Logger('MS-User:UserSignUpHandler');

@CommandHandler(UserSignUpCommand)
export class UserSignUpHandler implements ICommandHandler<UserSignUpCommand> {

  constructor(
    @Inject(EventPublisher)
    private readonly publisher: EventPublisher,
    private readonly userDomain: UserDomain,
  ){}

  async execute({ signUpData }: UserSignUpCommand) {
    logger.debug('execute OK')

    const user = this.publisher.mergeObjectContext(this.userDomain);

    user.create(signUpData);
    user.commit();
  }
}
