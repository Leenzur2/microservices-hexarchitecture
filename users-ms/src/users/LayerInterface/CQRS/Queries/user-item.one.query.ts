import { UserItemByIdInputDTO } from "../../DTOs/user-item-by-id.input.dto";

export class UserItemOneQuery {
  constructor(
    public readonly inputDTO: UserItemByIdInputDTO,
  ) {}
}