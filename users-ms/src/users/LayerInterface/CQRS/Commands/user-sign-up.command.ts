import { UserSignUpInputDTO } from "../../DTOs/user-sign-up.input.dto";

export class UserSignUpCommand {
  constructor(
    public readonly signUpData: UserSignUpInputDTO,
  ) {}
}
  