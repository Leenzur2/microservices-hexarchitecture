import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { validate, ValidationError } from 'class-validator';
import { UserSignUpInputDTO } from './DTOs/user-sign-up.input.dto';
import { UserItemOutputDTO } from './DTOs/user-item.output.dto'
// import ValidationPipe from 'src/validation.pipe';
import { UserItemByIdInputDTO } from './DTOs/user-item-by-id.input.dto';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { UserItemAllQuery } from './CQRS/Queries/user-item.all.query';
import { UserItemOneQuery } from './CQRS/Queries/user-item.one.query';
import { UserSignUpCommand } from './CQRS/Commands/user-sign-up.command';

const logger = new Logger('MS-User:UserController');

@Controller('users')
export class UserController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @MessagePattern({cmd: 'ms-users-get-all-users'})
  async getAllUsers(): Promise<UserItemOutputDTO[]> {
    logger.debug('getAllUsers query');
    return this.queryBus.execute(new UserItemAllQuery());
  }

  @MessagePattern({cmd: 'ms-users-get-one-user'})
  async getOneUser(@Payload() data: string): Promise<UserItemOutputDTO|ValidationError[]> {
    logger.debug('getOneUser query');
    logger.debug(`execute UserSignUpCommand - data : ${data}`)
    const input = JSON.parse(data) as UserItemByIdInputDTO;

    const errors = await validate(input);
    if (errors.length > 0) {
      return errors;
    }

    return this.queryBus.execute(new UserItemOneQuery(input));
  }

  @MessagePattern({cmd: 'ms-users-signup-user'})
  async signUp(@Payload() data: string): Promise<string|ValidationError[]> {

    logger.debug('signUp requested')
    logger.debug(`execute UserSignUpCommand - data : ${data}`)
    const input = JSON.parse(data) as UserSignUpInputDTO;

    const errors = await validate(input);
    if (errors.length > 0) {
      return errors;
    }
    
    await this.commandBus.execute(new UserSignUpCommand(input));

    return 'Send';
  }
}
