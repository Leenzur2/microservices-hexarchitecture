import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { UserEntity } from './users/LayerCore/user.entity';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    })
    ,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT, 10),
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [UserEntity],
      autoLoadEntities: false,
      synchronize: (process.env.NODE_ENV !== 'prod'),
      retryAttempts: (process.env.NODE_ENV === 'prod') ? 2 : 10,
      retryDelay: (process.env.NODE_ENV === 'prod') ? 1000 : 3000,
      logging: ['error'],
    }),
    UsersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
