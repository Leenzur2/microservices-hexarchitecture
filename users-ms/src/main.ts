
import { NestFactory } from '@nestjs/core';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';

const logger = new Logger('MS-User');

async function bootstrap() {
  const rabbitmqUrl = `amqp://${process.env.RABBITMQ_HOSTNAME}:${process.env.RABBITMQ_PORT}`;

  const app = await NestFactory.create(AppModule);

  await app.connectMicroservice<MicroserviceOptions>({
      transport: Transport.RMQ,
      options: {
        urls: [rabbitmqUrl],
        queue: 'ms-users-service_queue',
        queueOptions: {
          durable: false
        },
      },
    },
  );
  
  await app.startAllMicroservices();
  await app.init();
  process.on('unhandledRejection', (reason) => logger.error(reason))
}
bootstrap();
