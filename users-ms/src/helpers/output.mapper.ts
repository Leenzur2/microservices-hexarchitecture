import { Injectable } from "@nestjs/common";

@Injectable()
export class OutputMapper<Entity, DTO> {

    map(entity: Entity, dto: DTO): DTO {
        for (const property in dto) {
            if (!Object.prototype.hasOwnProperty.call(entity, property)) {
                throw new Error(`Trying to map <DTO> with <Entity> but ${property} property not existing in entity`)
            }
            dto[property as Extract<keyof any, string>] = entity[property as Extract<keyof any, string>];
        }
        return dto;
    }
}