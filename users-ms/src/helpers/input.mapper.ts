import { Injectable } from "@nestjs/common";

@Injectable()
export class InputMapper<DTO, Entity> {

    map(dto: DTO, entity: Entity): Entity {
        for (const property in dto) {
            if (!Object.prototype.hasOwnProperty.call(entity, property)) {
                throw new Error(`Trying to map <DTO> with <Entity> but ${property} property not existing in entity`)
            }
            entity[property as Extract<keyof any, string>] = dto[property as Extract<keyof any, string>];
        }
        return entity;
    }
}